<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="styLesheet" type="text/css" href="css/app.css" />
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container mt-3">
		<h2>connexion</h2>
		<form action="" method="post">

			<div class="mb-3 mt-3">
				<label class="form-label"> Nom </label>
				<input	class="form-control form-control-sm" type="text" placeholder="Nom" name="nom">
			</div>
			<div class="mb-3 mt-3">
				<label for="email">Email:</label> 
				<input type="email"	class="form-control" placeholder="Enter email" name="email">
			</div>
			<div class="mb-3">
				<label for="pwd">Password:</label> 
				<input type="password"	class="form-control"  placeholder="Enter password"	name="pswd">
			</div>
			<div class="form-check mb-3">
				<label class="form-check-label"> 
				<input	class="form-check-input" type="checkbox" name="remember">
					Remember me	</label>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>

	</form>
</body>
</html>