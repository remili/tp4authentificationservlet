package controlleurs;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import models.Utilisateur;

@WebServlet("/compte")
public class CompteControlleur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CompteControlleur() {
		super();

	}

	public void init(ServletConfig config) throws ServletException {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur user = (Utilisateur) session.getAttribute("user");
		String email = user.getEmail();
		if (email == null) {
			request.getRequestDispatcher("WEB-INF/compte/compte.jsp").forward(request, response);
		} else {
		}
		request.getRequestDispatcher("WEB-INF/compte/connexion.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
